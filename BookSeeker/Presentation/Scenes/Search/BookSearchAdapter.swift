//
//  BookSearchAdapter.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 24/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//
import UIKit
import RxSwift
import RxCocoa

class BookSearchAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {
    var tableView: UITableView
    var dataSource: [String] = []

    private let onKeywordSelectedSubject: PublishSubject<String> = PublishSubject()
    var onKeywordSelected: Observable<String> { return onKeywordSelectedSubject }

    init(tableView: UITableView) {
        self.tableView = tableView
        super.init()
        self.tableView.register(BookCell.nib, forCellReuseIdentifier: BookCell.reuseIdentifier)
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    func setData(_ data: [String]) {
        dataSource = data
        tableView.reloadData()
    }
}

extension BookSearchAdapter {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = dataSource[indexPath.row]
        cell.textLabel?.font = .boldSystemFont(ofSize: 24)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onKeywordSelectedSubject.onNext(dataSource[indexPath.row])
    }
}
