//
//  BooksListViewController.swift
//  BookSeeker
//
//  Created by user165657 on 21/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//

import UIKit
import RxSwift

protocol BooksListView: class, LoadingDisplayer, ErrorDisplayer {
    func displayBooksList(books: [BookSummary])
}

class BooksListViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    var adapter: BooksListAdapter!
    var presenter: BooksListPresenter!
    var keywords: String = ""
    var cached: Bool = false
    let disposeBag = DisposeBag()
    convenience init(keywords: String, fromCache: Bool) {
        self.init()
        self.keywords = keywords
        cached = fromCache
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        title = R.string.localizable.search_results()
        adapter = BooksListAdapter(tableView: tableView)
        presenter = BooksListPresenter(view: self)
        setupObservables()
        presenter.retrieveBooks(with: keywords, fromCache: cached)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setDefaultNavigationBarApperance()
        if #available(iOS 11.0, *) {
              navigationItem.largeTitleDisplayMode = .never
        }
    }
    func setupObservables() {
        adapter.onBookSelected.subscribe(onNext: { [unowned self] selectedBookId in
            let viewController = BookDetailsViewController(bookId: selectedBookId)
            self.navigationController?.pushViewController(viewController, animated: true)
        }).disposed(by: disposeBag)
    }
}

extension BooksListViewController: BooksListView {
    func displayBooksList(books: [BookSummary]) {
        tableView.isHidden = books.isEmpty
        if !books.isEmpty {
            adapter.setData(books)
        } else {
            displayEmptyResultError()
        }
    }
    func onTryAgainAction() {
        presenter.retrieveBooks(with: self.keywords, fromCache: self.cached)
    }
}
