//
//  BookListPresenter.swift
//  BookSeeker
//
//  Created by user165657 on 21/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//
import UIKit
import RxSwift

protocol BooksListPresenterProtocol {
    func retrieveBooks(with keywords: String, fromCache: Bool)
}

class BooksListPresenter {
    weak var view: BooksListView?
    var booksRepository: BooksRepository
    let disposeBag = DisposeBag()

    init(view: BooksListView) {
        self.view = view
        booksRepository = BooksRepository()
    }
}

extension BooksListPresenter: BooksListPresenterProtocol {
    func retrieveBooks(with keywords: String, fromCache: Bool) {
        view?.startLoading()
        booksRepository.getBooksList(with: keywords, fromCache: fromCache).do(onSuccess: { [weak self] books in
            self?.view?.stopLoading()
            self?.view?.displayBooksList(books: books)
        }, onError: { _ in
            self.view?.stopLoading()
            self.view?.displayGenericErrorMessage()
        }).subscribe().disposed(by: disposeBag)
    }
}
