//
//  BookListAdapter.swift
//  BookSeeker
//
//  Created by user165657 on 21/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//
import UIKit
import RxSwift
import RxCocoa

class BooksListAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {

    var tableView: UITableView
    var dataSource: [BookSummary] = []

    private let onBookSelectedSubject: PublishSubject<String> = PublishSubject()
    var onBookSelected: Observable<String> { return onBookSelectedSubject }

    init(tableView: UITableView) {
        self.tableView = tableView
        super.init()
        self.tableView.register(BookCell.nib, forCellReuseIdentifier: BookCell.reuseIdentifier)
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    func setData(_ data: [BookSummary]) {
        dataSource = data
        tableView.reloadData()
    }
}

extension BooksListAdapter {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: BookCell.reuseIdentifier,
                                                       for: indexPath) as? BookCell
        else {
            return UITableViewCell()
        }
        cell.configure(with: dataSource[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onBookSelectedSubject.onNext(dataSource[indexPath.row].bookId)
    }
}
