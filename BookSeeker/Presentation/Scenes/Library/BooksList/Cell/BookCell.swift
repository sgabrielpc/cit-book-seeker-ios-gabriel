//
//  BookCell.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 22/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//

import UIKit
import Kingfisher

class BookCell: UITableViewCell {
    @IBOutlet var bookCoverImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var authorLabel: UILabel!

    func configure(with book: BookSummary) {
        titleLabel.text = book.title
        authorLabel.text = book.author
        if let imageURL = book.coverImageURL {
            bookCoverImageView.kf.setImage(with: URL(string: imageURL))
        }
    }
}
