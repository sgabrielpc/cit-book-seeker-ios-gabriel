//
//  BookDetailsViewController.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 22/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//

import UIKit
import Cosmos
import Kingfisher

protocol BookDetailsView: class, LoadingDisplayer, ErrorDisplayer {
    func displayBookDetails(bookDetails: BookDetails)
}

class BookDetailsViewController: UIViewController {
    @IBOutlet var coverImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var authorLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var genresLabel: UILabel!
    @IBOutlet var starRatingWrapperStackView: UIStackView!
    @IBOutlet var starRatingComponent: CosmosView!
    @IBOutlet var starRatingLabel: UILabel!
    @IBOutlet var separator: UIView!
    @IBOutlet var descriptionLabel: UILabel!
    var bookId: String!
    var presenter: BookDetailsPresenter!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = R.string.localizable.book_details()
        presenter = BookDetailsPresenter(view: self)
        guard let bookId = bookId else { print("No bookId for details"); return }
        presenter.retrieveBookDetails(with: bookId)
    }

    convenience init(bookId: String) {
        self.init()
        self.bookId = bookId
    }
}

extension BookDetailsViewController: BookDetailsView {
    func displayBookDetails(bookDetails: BookDetails) {
        titleLabel.text = bookDetails.title
        authorLabel.text = bookDetails.author
        priceLabel.text = bookDetails.formattedPrice
        genresLabel.text = bookDetails.genres.joined(separator: ", ")
        if let imageURL = bookDetails.coverImageURL {
            coverImageView.kf.setImage(with: URL(string: imageURL))
        }
        if let rating = bookDetails.rating {
            starRatingComponent.rating = rating
            starRatingLabel.text = "\(rating)/5.0"
            starRatingComponent.settings.fillMode = .half
        } else {
            starRatingWrapperStackView.isHidden = true
        }
        if !bookDetails.description.isEmpty {
             descriptionLabel.attributedText = bookDetails.description.convertHtmlToNSAttributedString
        } else {
            separator.isHidden = true
            descriptionLabel.isHidden = true
            descriptionLabel.text = ""
        }
    }
    func onTryAgainAction() {
        presenter.retrieveBookDetails(with: bookId)
    }
}
