//
//  BookDetailsPresenter.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 22/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//

import UIKit
import RxSwift

protocol BookDetailsPresenterProtocol {
    func retrieveBookDetails(with bookId: String)
}

class BookDetailsPresenter {
    weak var view: BookDetailsView?
    var booksRepository: BooksRepository
    let disposeBag = DisposeBag()
    init(view: BookDetailsView) {
        self.view = view
        booksRepository = BooksRepository()
    }
}

extension BookDetailsPresenter: BookDetailsPresenterProtocol {
    func retrieveBookDetails(with bookId: String) {
        view?.startLoading()
            booksRepository.getBookDetails(with: bookId).do(onSuccess: { [weak self] details in
               self?.view?.stopLoading()
               self?.view?.displayBookDetails(bookDetails: details)
            }, onError: { _ in
               self.view?.stopLoading()
               self.view?.displayGenericErrorMessage()
        }).subscribe().disposed(by: disposeBag)
    }
}
