//
//  BooksRepository.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 22/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//

import RxSwift

struct BooksRepository {
    let booksRemoteDataSource = BooksRemoteDataSource()
    let booksCacheDataSource = BooksCacheDataSource()
    func getBooksList(with keywords: String, fromCache: Bool) -> Single<[BookSummary]> {
        if fromCache {
            return booksCacheDataSource.getSearchResult(with: keywords).map { ($0?.toBookSummaryList() ?? []) }
        }
        return booksRemoteDataSource.getBooks(with: keywords)
            // Converts the RemoteModel to the Cache Model
            .map { $0.toCacheModel(keywordsUsed: keywords) }
            .flatMap { searchResultCM in
                // Saves the Search Result in Cache model to cache
                self.booksCacheDataSource.saveToCache(searchResultCM)
                    // saveToCache returns a Completable, the function returns a Single,
                    // this makes it so it returns a Single of SearchResultCM
                    .andThen(Single.just(searchResultCM))
            // Converts from Cache Model to Domain Model (Array of Book Summary)
            }.map { $0.toBookSummaryList() }
    }
    func getBookDetails(with bookId: String) -> Single<BookDetails> {
        return booksCacheDataSource.getBook(with: bookId).map { $0.toBookDetails() }
    }
    func getRecentKeywordsUsed() -> Single<[String]> {
        return booksCacheDataSource.getRecentKeywordsUsed()
    }
}
