//
//  ResponseRM.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 22/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//

struct SearchResultRM: Decodable {
    let books: [BookRM]
    enum CodingKeys: String, CodingKey {
        case books = "results"
    }
}

extension SearchResultRM {
    func toCacheModel(keywordsUsed: String) -> SearchResultCM {
        return SearchResultCM(books: books.toCacheModel(), keywords: keywordsUsed)
    }
}
