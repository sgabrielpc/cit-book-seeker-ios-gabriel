//
//  BookSummaryRM.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 22/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//
import Foundation

struct BookRM: Decodable {
    let bookId: Int
    let title: String
    let author: String
    let description: String?
    let coverImageURL: String?
    let bigCoverImageURL: String?
    let price: Double?
    let formattedPrice: String?
    let averageUserRating: Double?
    let genres: [String]

    enum CodingKeys: String, CodingKey {
        case bookId = "trackId"
        case title = "trackName"
        case author = "artistName"
        case description = "description"
        case coverImageURL = "artworkUrl60"
        case bigCoverImageURL = "artworkUrl100"
        case price = "price"
        case formattedPrice = "formattedPrice"
        case averageUserRating = "averageUserRating"
        case genres = "genres"
    }
}

extension BookRM {
    func toCacheModel() -> BookCM {
        return BookCM(bookId: String(bookId),
                      title: title,
                      author: author,
                      description: description ?? "",
                      coverImageURL: coverImageURL,
                      bigCoverImageURL: bigCoverImageURL,
                      formattedPrice: formattedPrice ?? (price != nil ? String(format: "$%.2f", price!) : ""),
                      rating: averageUserRating,
                      genres: genres)
    }
}

extension Array where Element == BookRM {
    func toCacheModel() -> [BookCM] {
        return map { $0.toCacheModel() }
    }
}
