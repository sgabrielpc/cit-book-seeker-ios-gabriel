//
//  BooksRemoteDataSource.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 22/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//
import Foundation
import Moya
import RxSwift

struct BooksRemoteDataSource {
    let provider = MoyaProvider<BooksService>()
    func getBooks(with keywords: String) -> Single<SearchResultRM> {
        return provider.rx.request(.getBooks(keywords: keywords)).map(SearchResultRM.self)
    }
    /*
    // Provider with debug plugin
    static let requestDataFormatter = { String(data: $0, encoding: .utf8) ?? " "}
    static let moyaNetworkLoggerPlugin = NetworkLoggerPlugin(verbose: true, requestDataFormatter: requestDataFormatter)
    let provider = MoyaProvider<BooksService>(
        endpointClosure: { MoyaProvider.defaultEndpointMapping(for: $0) },
        plugins: [moyaNetworkLoggerPlugin]
    )
    */
}
