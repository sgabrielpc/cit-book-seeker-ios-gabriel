//
//  BooksService.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 22/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//

import Foundation
import Moya

enum BooksService {
    case getBooks(keywords: String)
}

extension BooksService: TargetType {
    var baseURL: URL {
        return URL(string: "https://itunes.apple.com/")!
    }

    var path: String {
        switch self {
        case .getBooks: return "search"
        }
    }

    var method: Moya.Method {
        switch self {
        case .getBooks:
            return .get
        }
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case .getBooks(let keywords):
            return .requestParameters(parameters: ["term": keywords,
                                                   "entity": "ibook"],
                                      encoding: URLEncoding.queryString)
        }
    }

    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}
