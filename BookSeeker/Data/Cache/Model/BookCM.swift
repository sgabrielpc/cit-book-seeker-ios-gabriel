//
//  BookCM.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 22/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//
import SwiftyUserDefaults

struct BookCM: Codable, DefaultsSerializable {
    let bookId: String
    let title: String
    let author: String
    let description: String
    let coverImageURL: String?
    let bigCoverImageURL: String?
    let formattedPrice: String
    let rating: Double?
    let genres: [String]
    enum CodingKeys: String, CodingKey {
        case bookId
        case title
        case author
        case description
        case coverImageURL
        case bigCoverImageURL
        case formattedPrice
        case rating
        case genres
    }
}

extension BookCM {
    func toBookSummary() -> BookSummary {
        return BookSummary(bookId: String(bookId), title: title, author: author, coverImageURL: coverImageURL)
    }
    func toBookDetails() -> BookDetails {
        return BookDetails(coverImageURL: bigCoverImageURL,
                           title: title,
                           author: author,
                           description: description,
                           formattedPrice: formattedPrice,
                           genres: genres,
                           rating: rating)
    }
}

extension Array where Element == BookCM {
    func toBookSummaryList() -> [BookSummary] {
        return map { $0.toBookSummary() }
    }
}
