//
//  SearchResultCM.swift
//  BookSeeker
//
//  Created by user165657 on 22/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//
import SwiftyUserDefaults

struct SearchResultCM: Codable, DefaultsSerializable {
    let books: [BookCM]
    let keywords: String
    enum CodingKeys: String, CodingKey {
        case books
        case keywords
    }
}

extension SearchResultCM {
    func toBookSummaryList() -> [BookSummary] {
        return books.toBookSummaryList()
    }
}
