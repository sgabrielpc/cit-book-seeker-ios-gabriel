//
//  BooksCacheDataSource.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 22/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//
import RxSwift
import SwiftyUserDefaults

struct BooksCacheDataSource {
    func getSearchResult(with keywordsUsed: String) -> Single<SearchResultCM?> {
        // Manually creates an observable that does an action,
        // this action is to emmit the search result stored in cache that used the keywords requested
        return Observable.create({ (observer: AnyObserver<SearchResultCM?>) -> Disposable in
            // This is the action of the observable
            observer.onNext(Defaults.searchResults.first(where: { $0.keywords == keywordsUsed}))
            return Disposables.create()
        }).take(1).asSingle()
    }
    func getBook(with bookId: String) ->  Single<BookCM> {
        return Observable.create({ (observer: AnyObserver<BookCM>) -> Disposable in
            // This will always succeed because it's the search we just did with a bookId that we just clicked
            let bookWithRequestedId = Defaults.searchResults.last?.books.first(where: { $0.bookId == bookId })
            observer.onNext(bookWithRequestedId!) // So we can safely force-unwrap this
            return Disposables.create()
        }).take(1).asSingle()
    }
    func getRecentKeywordsUsed() -> Single<[String]> {
        return Observable.create({ (observer: AnyObserver<[String]>) -> Disposable in
            observer.onNext(Defaults.searchResults.map { $0.keywords })
            return Disposables.create()
        }).take(1).asSingle()
    }
    func saveToCache(_ searchResult: SearchResultCM) -> Completable {
        return Completable.empty().do(onCompleted: {
            if !Defaults.searchResults.contains(where: {$0.keywords.lowercased()==searchResult.keywords.lowercased()}) {
                Defaults.searchResults.append(searchResult)
                // Saves only the 6 last searches to avoid too much storage consumption
                if Defaults.searchResults.count > 6 {
                  Defaults.searchResults.remove(at: 0)
                }
            } else {
                Defaults.searchResults.removeAll(where: {$0.keywords.lowercased()==searchResult.keywords.lowercased()})
                Defaults.searchResults.append(searchResult)
            }
        })
    }
}

extension DefaultsKeys {
    var searchResults: DefaultsKey<[SearchResultCM]> { return .init("SeachResults", defaultValue: []) }
}
