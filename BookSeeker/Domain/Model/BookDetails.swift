//
//  BookDetails.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 22/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//
import Foundation

struct BookDetails {
    let coverImageURL: String?
    let title: String
    let author: String
    let description: String
    let formattedPrice: String
    let genres: [String]
    let rating: Double?
}
