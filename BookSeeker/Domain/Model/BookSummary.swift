//
//  BookSummary.swift
//  BookSeeker
//
//  Created by user165657 on 21/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//
struct BookSummary {
    let bookId: String
    let title: String
    let author: String
    let coverImageURL: String?
}
