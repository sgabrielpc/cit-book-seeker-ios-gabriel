//
//  BookSearchViewController.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 23/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//

import UIKit
import RxSwift

protocol BookSearchView: class, LoadingDisplayer, ErrorDisplayer {
    func displayRecentSearchesList(keywords: [String])
}

class BookSearchViewController: UIViewController {
    @IBOutlet var keywordsTableView: UITableView!
    @IBOutlet var recentSearchesLabel: UILabel!
    @IBOutlet var emptyRecentSearchesMessageLabel: UILabel!
    var presenter: BookSearchPresenter!
    var adapter: BookSearchAdapter!
    let disposeBag = DisposeBag()
    let searchController = UISearchController(searchResultsController: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        title = R.string.localizable.search()
        setupLayout()
        adapter = BookSearchAdapter(tableView: keywordsTableView)
        setupObservables()
        presenter = BookSearchPresenter(view: self)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setLargeNavigationBarApperance()
        if #available(iOS 11.0, *) {
              navigationItem.largeTitleDisplayMode = .always
        }
        presenter.retrieveRecentSearches()
    }
    func setupLayout() {
        recentSearchesLabel.text = R.string.localizable.recent_searches()
        emptyRecentSearchesMessageLabel.text = R.string.localizable.empty_recent_searches()
        searchController.searchBar.placeholder = R.string.localizable.apple_books()
        navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
    }
    func setupObservables() {
        adapter.onKeywordSelected.subscribe(onNext: { [unowned self] keywords in
            let viewController = BooksListViewController(keywords: keywords, fromCache: true)
            self.navigationController?.pushViewController(viewController, animated: true)
        }).disposed(by: disposeBag)
    }
}

extension BookSearchViewController: BookSearchView {
    func displayRecentSearchesList(keywords: [String]) {
        if !keywords.isEmpty {
            emptyRecentSearchesMessageLabel.isHidden = true
            keywordsTableView.isHidden = false
            adapter.setData(keywords.reversed())
        } else {
            emptyRecentSearchesMessageLabel.isHidden = false
            keywordsTableView.isHidden = true
        }
   }
}

extension BookSearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchBarText = searchBar.text else { return }
        let viewController = BooksListViewController(keywords: searchBarText, fromCache: false)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
