//
//  LoadingDisplayer.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 23/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//

import UIKit

protocol LoadingDisplayer {
    func startLoading()
    func stopLoading()
}

extension LoadingDisplayer where Self: UIViewController {
    func startLoading() {
        if view.viewWithTag(1234) != nil { return }
        let loadingView = UIView()
        loadingView.isAccessibilityElement = true
        loadingView.accessibilityLabel = R.string.localizable.loading()
        loadingView.backgroundColor = UIColor.black
        loadingView.alpha = 0.5
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        loadingView.isHidden = true
        view.addSubview(loadingView)
        loadingView.addSubview(activityIndicator)
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        loadingView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        loadingView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        loadingView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: loadingView.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: loadingView.centerYAnchor).isActive = true
        loadingView.tag = 1234
        activityIndicator.startAnimating()
        view.bringSubviewToFront(activityIndicator)
        loadingView.isHidden = false
    }
    func stopLoading() {
        if let activityIndicator = view.viewWithTag(1234) {
            activityIndicator.removeFromSuperview()
        }
    }
}
