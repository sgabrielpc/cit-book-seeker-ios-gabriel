//
//  NibLoadable.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 22/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//

import UIKit

protocol NibLoadable: class {}

// Every UIView has the methods defined in NibLoadable
extension UIView: NibLoadable {}

extension NibLoadable where Self: UIView {

    // Instead of every time having to write the name of the class to use in the dequeueReusableCell
    // we can just call this that resolves to the class name
    static var reuseIdentifier: String {
        return String(describing: self)
    }

    // Makes it easy to register the cell for the TableView
    static var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    // Used to load UIViews and strech it on the full screen
    func loadNibContent() {
        let layoutAttributes: [NSLayoutConstraint.Attribute] = [.top, .leading, .bottom, .trailing]
        for view in Self.nib.instantiate(withOwner: self, options: nil) {
            if let view = view as? UIView {
                view.translatesAutoresizingMaskIntoConstraints = false
                self.addSubview(view)
                layoutAttributes.forEach { attribute in
                    self.addConstraint(NSLayoutConstraint(item: view,
                                                          attribute: attribute,
                                                          relatedBy: .equal,
                                                          toItem: self,
                                                          attribute: attribute,
                                                          multiplier: 1,
                                                          constant: 0.0))
                }
            }
        }
    }
}
