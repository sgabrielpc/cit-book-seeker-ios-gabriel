//
//  ErrorView.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 23/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class ErrorView: UIView {
    @IBOutlet var errorView: UIView!
    @IBOutlet var errorIcon: UIImageView!
    @IBOutlet var errorMessage: UILabel!
    @IBOutlet var tryAgainButton: UIButton!
    private var onClickedTryAgainSubject = PublishSubject<Void>()
    var onClickedTryAgain: Observable<Void> { return onClickedTryAgainSubject }
    let disposebag = DisposeBag()
    override init(frame: CGRect) {
       super.init(frame: frame)
        loadNibContent()
    }

    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        loadNibContent()
    }

    override func prepareForInterfaceBuilder() {
       super.prepareForInterfaceBuilder()
        loadNibContent()
    }
    func configure(message: String, icon: UIImage?) {
        errorMessage.text = message
        if icon != nil {
            errorIcon.image = icon
        }
        tryAgainButton.setTitle(R.string.localizable.try_again(), for: .normal)
        tryAgainButton.rx.tap.asObservable()
           .do(onNext: { [unowned self] _ in self.removeFromSuperview() })
           .bind(to: onClickedTryAgainSubject)
           .disposed(by: disposebag)
    }
}

protocol ErrorDisplayer {
    var viewToDisplayErrorInto: UIView { get }
    func displayEmptyResultError()
    func displayGenericErrorMessage()
    func onTryAgainAction()
}

extension ErrorDisplayer where Self: UIViewController {
    // Displays the error in the root view if the class that is implementing
    // the protocol doesn't specofy any other place that it wants the error to be shown
    var viewToDisplayErrorInto: UIView {
        return view
    }
    func displayEmptyResultError() {
        return displayError(message: R.string.localizable.empty_search_result())
    }
    func displayGenericErrorMessage() {
        return displayError(message: R.string.localizable.unexpected_error())
    }
    private func displayError(message: String, icon: UIImage? = nil) {
        let emptyView = ErrorView()
        viewToDisplayErrorInto.addSubview(emptyView)
        emptyView.snp.makeConstraints { $0.edges.equalToSuperview() }
        emptyView.configure(message: message, icon: icon)
        viewToDisplayErrorInto.bringSubviewToFront(emptyView)
        emptyView.onClickedTryAgain
        .subscribe(onNext: { [unowned self] _ in self.onTryAgainAction() })
        .disposed(by: emptyView.disposebag)
    }
    func onTryAgainAction() {
        // Override this function to tell what to do when clicked the try again button
    }
}
