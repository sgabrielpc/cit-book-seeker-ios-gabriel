//
//  BookSearchPresenter.swift
//  BookSeeker
//
//  Created by Gabriel Pinheiro de Carvalho on 23/01/20.
//  Copyright © 2020 CIT. All rights reserved.
//

import UIKit
import RxSwift

protocol BookSearchPresenterProtocol {
    func retrieveRecentSearches()
}

class BookSearchPresenter {
    weak var view: BookSearchView? // Weak reference to the view so we can call methods to display the result.
    //It's weak because the view controller has a reference to the presenter and now the presenter has a reference
    //to the view, if it wasn't weak, it would never be unalocated.
    var booksRepository: BooksRepository
    let disposeBag = DisposeBag()

    init(view: BookSearchView) {
        self.view = view
        booksRepository = BooksRepository()
    }
}

extension BookSearchPresenter: BookSearchPresenterProtocol {
    func retrieveRecentSearches() {
        view?.startLoading()
        booksRepository.getRecentKeywordsUsed().do(onSuccess: { [unowned self] keywords in
            self.view?.stopLoading()
            self.view?.displayRecentSearchesList(keywords: keywords)
        }, onError: { _ in
            self.view?.stopLoading()
            self.view?.displayGenericErrorMessage()
        }).subscribe().disposed(by: disposeBag)
    }
}
